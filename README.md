# Double Examen Docker / Gitlab 2022-2023

La première partie de cet examen concerne Docker et le cours que nous avons eu ensemble il y a quelques semaines. La seconde partie de cet examen (à laquelle vous n'avez pas encore accès) concerne Gitlab (et donc Git), le cours que nous avons eu ensemble cette semaine.

## Règles de l'examen :
- Les téléphones sont interdits (si vous avez peur de ne pas leur résister, vous pouvez les poser sur mon bureau, vous aurez le droit à une pause entre les deux examens vous pourrez les utiliser à ce moment là)
- La communication entre vous via des outils en ligne (Discord, Teams, Email ou autre) est interdite, je vous invite à les couper pour éviter tout accident.
- La communication entre vous à l'oral est interdite.
- Si vous avez des questions sur l'énoncé, demandez-moi, vous ne serez sans doute pas les seuls à vous poser la question.
- Pour tous les exercices, vous pouvez utiliser les outils qui vous semblent nécessaires.
- Chaque exercice est un travail individuel.
- Vous pouvez "m'acheter" de l'aide pour vous débloquer ponctuellement, chaque aide apportée vous coûtera **deux points**. Utilisez ce joker intelligemment !

Tout manquement à ces règles provoquera un zero à l'examen et une convocation du côté de l'administration de Sup De Vinci.

## Rendu

- Pour rendre l'examen, vous devez créer un projet Gitlab qui s'appelle `exam-docker-dec-2022`
- A l'intérieur du projet je dois trouver
  - Un fichier `NOM` avec votre nom dedans
  - Un fichier `NOM_PRENOM_questions_cours_docker.txt` qui contient les réponses aux questions de cours (exercice 1)
  - Un fichier `Dockerfile` qui contient l'exercice 2
  - Un dossier `exercice-3` qui contient les sous-dossiers nécessaires demandés à l'exercice 3.
- Vous m'ajouterez dans le projet en tant que `Mainteneur` de votre projet. Mon nom d'utilisateur est @tsaquet. Ainsi j'aurais accès à tous vos commits et à tout votre travail. 

## Docker (Durée totale 1h30 - Début 13h40)

### Exercice 1 - Question cours (15 minutes - 4 pts)

Les questions sont à rendre à 13h55 au plus tard (heure du commit faisant foi), dans le fichier `NOM_PRENOM_questions_cours_docker.txt` **(.txt, pas .docx ou autre format inadéquat)** qui contient les questions (copiées ci-dessous) et les réponses dans les espaces laissés entre chaque question. 

- Chaque minute de retard vous coûte un point.  
- **Les réponses doivent tenir en une phrase.**  
- **Vous devez écrire les réponses vous même, avec vos mots.**  

#### Docker

- A quoi sert un conteneur ?

- Quelle est la commande qui permet au démon Docker de démarrer avec la machine ?

- Comment lister les conteneurs qui sont en marche ?

- Comment lister les conteneurs qui sont arrêtés ?

- Quel logiciel permet de décrire un ensemble de services dans un fichier yaml afin de les gérer simultanément ?

- A quoi sert un volume ?

#### Images

- Qu'est-ce qu'un Dockerfile ?

- A quoi sert le mot clé RUN dans un Dockerfile ?

- Comment construire une image docker de manière à maximiser l'utilisation du cache ?

- L'instruction EXPOSE permet de publier le port. Est-ce que cette phrase est correcte ?

- Quelles sont les contraintes pour ajouter une image dans une registry privée ?

- Chaque couche d'une image est constituée des différences avec les couches situées avant. Les couches sont empilées les unes sur les autres. Est-ce que cette phrase à propos d'une image est correcte ?

- Qu'est-ce que le copy-on-write (CoW) ?

#### Conteneurs

- Comment utilise-t-on l'option pour mapper le port d'un conteneur sur un port de l'hôte ?

- Comment utilise-t-on l'option pour mapper un volume d'un conteneur sur un répertoire de l'hôte ?

- Quelle commande permet de démarrer un conteneur à partir d'une image ?

#### Orchestration

- A quoi sert un orchestrateur (comme Docker Swarm ou Kubernetes) ?

- A quoi sert le mode "replicas" de Docker Swarm ?

- A quoi sert le mode "global" de Docker Swarm ?

- Quelle commande permet d'augmenter le nombre de replicas ?

### Exercices pratiques

Utilisez votre temps comme vous le souhaitez pour répondre à ces deux exercices. La fin de l'examen Docker est à 15h10.

Vous perdez un point par 5 minutes de retard. C'est un travail individuel.

#### Exercice 2 - Commenter un Dockerfile (temps maximum recommandé 15 minutes - 3 pts)

Commentez chaque INSTRUCTION de ce fichier Dockerfile pour expliquer à quoi ça sert. Il y a 15 instructions qui sont précédées par des `#` qui indiquent où commenter.

```yaml
# Ceci est un commentaire.
# Il faut commenter chaque instruction qui se trouve dans ce fichier,
# en commençant par le FROM juste en dessous !

#
FROM ubuntu:20.04

#
MAINTAINER GitLab Inc. <support@gitlab.com>

#
SHELL ["/bin/sh", "-c"]

# 
ENV LANG=C.UTF-8

# 
RUN apt-get update -q \
    && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
      busybox \
      ca-certificates \
      openssh-server \
      tzdata \
      wget \
      perl \
      libperl5.30 \
    && rm -rf /var/lib/apt/lists/*

# 
ENV EDITOR /bin/vi
RUN busybox --install \
    && { \
        echo '#!/bin/sh'; \
        echo '/bin/vi "$@"'; \
    } > /usr/local/bin/busybox-editor \
    && chmod +x /usr/local/bin/busybox-editor \
    && update-alternatives --install /usr/bin/editor editor /usr/local/bin/busybox-editor 1

# 
RUN rm -rf /etc/update-motd.d /etc/motd /etc/motd.dynamic
RUN ln -fs /dev/null /run/motd.dynamic

# 
COPY RELEASE /
COPY assets/ /assets/

# 
RUN chmod -R og-w /assets RELEASE ; \
  /assets/setup

# 
ENV PATH /opt/gitlab/embedded/bin:/opt/gitlab/bin:/assets:$PATH

# 
ENV TERM xterm

# 
EXPOSE 443 80 22

# 
VOLUME ["/etc/gitlab", "/var/opt/gitlab", "/var/log/gitlab"]

# 
CMD ["/assets/wrapper"]

#
HEALTHCHECK --interval=60s --timeout=30s --retries=5 \
CMD /opt/gitlab/bin/gitlab-healthcheck --fail --max-time 10

```

#### Exercice 3 - Créer le docker-compose qui permet de faire traefik + node red (13 pts - fin à 15h10)

##### Objectif

L'objectif est de mettre à disposition plusieurs instances de Node-Red, un outil d'automatisation basé sur un système de noeuds en low code. (C'est à dire qu'en principe il n'y a pas besoin de coder pour utiliser cet outil, ça peut arriver pour certains cas précis / complexes. C'est un outil qui peut être utilisé pour faire de la domotique, ou encore pour automatiser les actions d'un live twitch, il est très polyvalent.)

Pour réaliser ça dans de bonnes conditions, nous souhaitons placer une ou plusieurs instances de Node-Red derrière un Reverse-Proxy. Pour cela, nous allons utiliser **Traefik** et **Docker Compose**.

##### Traefik

Traefik est donc un reverse-proxy, un outil qui permet d'associer un service à une adresse passée en entrée dans un navigateur. En fonction de certains critères, Traefik affichera tel ou tel "backend".  

Nous allons procéder par étapes.  

**Attention** aux éléments que vous allez trouver en ligne, Traefik a deux versions (v1 et v2) et les instructions ont évolué entre les deux. Attention à utiliser la **version 2** et à ne pas utiliser d'instruction de la version 1.  

**Important**

Pour chaque étape, faites un **nouveau** répertoire dans lequel se trouveront les fichiers qui permettent de répondre. Vous pouvez évidemment copier les fichiers des répertoires précédents si vous en avez à nouveau besoin.  

**Attention** : n'oubliez pas d'inclure le fichier hosts si vous devez le modifier !  

##### Etape 1 : Traefik

_Objectif_ : En utilisant Docker Compose, mettre en place Traefik et afficher son Dashboard sur votre localhost, port 31983.
Vous pouvez trouver toutes les instructions nécessaires dans la documentation de Traefik

##### Etape 2 : Whoami ?

_Objectif_ : Toujours à l'aide de Docker Compose, mettre en place l'outil "whoami" de Traefik. Il doit s'afficher lorsque vous entrez une adresse formatée comme ceci : votreprenom.votrenom.local
Vous pouvez trouver toutes les instructions nécessaires dans la documentation de Traefik

##### Etape 3 : Node-Red tout seul

_Objectif_ : Faire démarrer un conteneur NodeRed tout seul
Vous pouvez trouver toutes les instructions nécessaires dans la documentation de l'image docker NodeRed.

##### Etape 4 : Plusieurs Node-Red derrière Traefik

_Objectif_ : Toujours à l'aide de Docker Compose, faire démarrer 3 conteneurs node-red derrière Traefik qui répondront aux adresses (à travers Traefik)
- node1.votrenom.local
- node2.votrenom.local
- node3.votrenom.local
Pour cette question il va être nécessaire de combiner les informations trouvées dans les questions précédentes.

#### Question "points en plus"

_Objectif_ : Déployer portainer à partir d'un fichier `docker-compose.yml` :
- sur le port 9000 si vous n'avez pas réussi à faire marcher Traefik
- sur l'URL portainer.local si tout est ok avec Traefik

## Pause (15 minutes - retour à 15h25)
